<?php
require_once 'database.php';

function get_recent_messages($id) {
    $conn = connect();

    if ($id)
    {
        $result = $conn->query("call get_messages_after($id)");
    }
    else {
        $result = $conn->query("call get_recent_messages()");
    }

    if($result) {
        $msgs = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Error retrieving messages", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $msgs;
}

function add_message($username, $message) {
    $conn = connect();
    $result = $conn->query("call add_message('$username', '$message')");
    
    if($result) {
        $msg = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Message could not be sent", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $msg[0];
}

function generateJSON($messages)
{
        if ($messages)
        {
            $out = array();

            foreach ($messages as $msg)
            {
                array_push($out, array(
                    "id"=>$msg["id"],
                    "sent"=>$msg["sent"],
                    "username"=>$msg["username"],
                    "message"=>$msg["content"]
                ));
            }

            echo json_encode($out);
        }
        else
        {
            send_msg("No message found");
        }
}

function send_msg($msg)
{
    echo json_encode(array(
        "id" => 0,
        "sent" => 0,
        "username" => "",
        "message" => $msg
    ));
}
?>