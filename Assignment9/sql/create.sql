/*
    TABLES
*/
CREATE TABLE messages (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50),
    sent TIMESTAMP,
    content VARCHAR(255),
    PRIMARY KEY (id)
);

/*
    STORED PROCEDURES
*/
DELIMITER //

CREATE PROCEDURE get_recent_messages ()
BEGIN

SELECT id, username, sent, content
FROM messages
WHERE sent >= (NOW() - INTERVAL 1 HOUR)
ORDER BY sent ASC
LIMIT 10;

END //

DELIMITER //

CREATE PROCEDURE get_messages_after (IN msg_id int)
BEGIN

SELECT id, username, sent, content
FROM messages
WHERE id > msg_id
ORDER BY sent ASC;

END //

DELIMITER //

CREATE PROCEDURE add_message (IN username varchar(50), IN content varchar(255))
BEGIN

INSERT INTO messages (username, content) VALUES (username, content);
SELECT id, sent, username, content FROM messages WHERE id = LAST_INSERT_ID();

END //