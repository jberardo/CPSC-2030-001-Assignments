<?php

function connect(){
    $user = 'root';
    $pwd = '';
    $server = 'localhost';
    $dbname = 'chat';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}

function clearConnection($conn){
    while($conn->more_results()){
       $conn->next_result();
       $conn->use_result();
    }
}

function show_sql_error($msg, $conn){
    echo $msg . "<br/>";
    echo mysqli_error($conn);
}

?>