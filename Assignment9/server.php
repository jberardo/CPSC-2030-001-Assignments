<?php

require_once 'functions.php';

// only acccept AJAX requests
if ( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' )
{
    // POST method with username and message
    // when user hits enter on the text box this if will be called
    // and add the message to the database
    if(array_key_exists("username", $_POST) and array_key_exists("message", $_POST))
    {
        // add message to database
        $message = add_message($_POST["username"], $_POST["message"]);
        $out["id"] = $message["id"];
        $out["sent"] = $message["sent"];
        $out["username"] = $message["username"];
        $out["message"] = $message["content"];
        echo json_encode($out);
    }
    // if we have and id from POST method
    // we will get all messages sent after the message 
    // that has the id passed
    else if (array_key_exists("id", $_POST))
    {
        // get all msgs after message with 'id'
        $messages = get_recent_messages($_POST["id"]);

        if ($messages)
        {
            generateJSON($messages);
        }
        else
        {
            send_msg("error getting messages from database");
        }
    }
    else
    {
        // get last 10 msgs in the last hour
        $messages = get_recent_messages(0);
        if ($messages)
        {
            generateJSON($messages);
        }
        else
        {
            send_msg("error getting messages from database");
        }

    }
} else {
    send_msg("AJAX calls only");
}
?>