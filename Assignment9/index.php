<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" href="style.less" />

    <script src="http://cdnjs.cloudflare.com/ajax/libs/less.js/2.7.2/less.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="script.js" defer></script>
</head>

<body>
    <div class="container">
        <div class="header">
            <h1>Chat</h1>
        </div>

        <div class="users">
            <div class="form">
                <span>Username<span>
                <input type="text" name="username" id="username">
            </div>
        </div>

        <div class="messages" id="messages">
        </div>

        <div class="message">
            <input type="text" name="msg" id="msg">
        </div>
    </div>
</body>

</html>