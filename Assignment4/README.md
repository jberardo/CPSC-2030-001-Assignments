# CPSC 2030 Assignment 4

## Database creation:

Please write a SQL creation script with the extension .sql:
- create a database called pokedex
- create a table called pokemon
    - Pay attention to how you will encode the mega evolution forms
    - Fill it with data at from entries 387 to 627 in the pokedex
    - Less than 70 Pokémon, no SQL creation portion will not be graded.
    - You can either use 1 table or 2 tables or 3 tables to represent the pokedex
    - optional, and a column with a URL to an image of the Pokémon
- Create tables to represent the type effectiveness and weaknesses
    - You may will need at least 4 tables to do this

## Clean up script:
Please create a SQL clean up script that deletes the database with the extension .sql

## Queries script:
Please create a script( once again .sql extension) with the following queries.
1.	Find the Bottom 10 Pokémon ranked by speed
2.	Find All Pokémon who is vulnerable to Ground and Resistant to Steel
3.	Find All Pokémon who has a BST between 200 to 500 who is weak against water types
4.	Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire

**Note you will be using this database in future assignments.**

## Submission
- Place all  SQL files in the Assignment 4 folder on your Git repository
- Make sure your SQL queries execute. Any SQL file that do not execute will get 0.
    - You will check this by uploading your SQL file in the PhPMyAdmin page and making sure there are no errors.
- Due Date: October 14th, 2018

# Pokemon

## Type
- deals additional/reduced damage
- can be
    - strong/weak
    - super effective/not very effective

## Type Chart

|Type|Strong Against|Weak Against|Resistant To|Vulnerable To|
|-|-|-|-|-|
|Normal|-|Rock, Ghost, Steel|Ghost|Fighting|
|Fighting|Normal, Rock, Steel, Ice, Dark|Flying, Poison, Psychic, Bug, Ghost, Fairy|Rock, Bug, Dark|Flying, Psychic, Fairy|
|Flying|Fighting, Bug, Grass|Rock, Steel, Electric|Fighting, Ground, Bug, Grass|Rock, Electric, Ice|
|Poison|Grass, Fairy|Poison, Ground, Rock, Ghost, Steel|Fighting, Poison, Grass, Fairy|Ground, Psychic|
|Ground|Poison, Rock, Steel, Fire, Electric|Flying, Bug, Grass|Poison, Rock, Electric|Water, Grass, Ice|
|Rock|Flying, Bug, Fire, Ice|Fighting, Ground, Steel|Normal, Flying, Poison, Fire|Fighting, Ground, Steel, Water, Grass|
|Bug|Grass, Psychic, Dark|Fighting, Flying, Poison, Ghost, Steel, Fire, Fairy|Fighting, Ground, Grass|Flying, Rock, Fire|
|Ghost|Ghost, Psychic|Normal, Dark|Normal, Fighting, Poison, Bug|Ghost, Dark|
|Steel|Rock, Ice, Fairy|Steel, Fire, Water, Electric|Normal, Flying, Poison, Rock, Bug, Steel, Grass, Psychic, Ice, Dragon, Fairy|Fighting, Ground, Fire|
|Fire|Bug, Steel, Grass, Ice|Rock, Fire, Water, Dragon|Bug, Steel, Fire, Grass, Ice|Ground, Rock, Water|
|Water|Ground, Rock, Fire|Water, Grass, Dragon|Steel, Fire, Water, Ice|Grass, Electric|
|Grass|Ground, Rock, Water|Flying, Poison, Bug, Steel, Fire, Grass, Dragon|Ground, Water, Grass, Electric|Flying, Poison, Bug, Fire, Ice|
|Electric|Flying, Water|Ground, Grass, Electric, Dragon|Flying, Steel, Electric|Ground|
|Psychic|Fighting, Poison|Steel, Psychic, Dark|Fighting, Psychic|Bug, Ghost, Dark|
|Ice|Flying, Ground, Grass, Dragon|Steel, Fire, Water, Ice|Ice|Fighting, Rock, Steel, Fire|
|Dragon|Dragon|Steel, Fairy|Fire, Water, Grass, Electric|Ice, Dragon, Fairy|
|Fairy|Fighting, Dragon, Dark|Poison, Steel, Fire|Fighting, Bug, Dragon, Dark|Poison, Steel|
|Dark|Ghost, Psychic|Fighting, Dark, Fairy|Ghost, Psychic, Dark|Fighting, Bug, Fairy|

# Pokedex

|Attribute|Name|Description|
|-|-|-|
|Nat.#|-|National Pokedex Number|
|Hoenn#|-|Hoenn Pokedex Number|
|Name|-|-|
|Type|-|-|
|HP|HP|Base HP for this pokemon|
|Atk|Attack|Base Attack for this pokemon|
|Def|Defense|Base Defense for this pokemon|
|SAt|Special Attack|Base Special Attack for this pokemon|
|SDf|Special Defense|Base Special Defense for this pokemon|
|Spd|Speed|Base Speed for this pokemon|
|BST|Base Stat Total|The sum of the Pokemon's base stat values|