1.	Find the Bottom 10 Pokémon ranked by speed
SELECT *
FROM pokemon
ORDER BY speed DESC
limit 10;

2.	Find All Pokémon who is vulnerable to Ground and Resistant to Steel

SELECT p.name
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
WHERE
(t1.id IN
    (SELECT target_type FROM type_resistant WHERE source_type = (SELECT id FROM types WHERE  name = 'Steel')
    UNION
    SELECT target_type FROM type_vulnerable WHERE source_type = (SELECT id FROM types WHERE  name = 'Ground'))
)
OR
(t2.id IN
    (SELECT target_type FROM type_resistant WHERE source_type = (SELECT id FROM types WHERE  name = 'Steel')
    UNION
    SELECT target_type FROM type_vulnerable WHERE source_type = (SELECT id FROM types WHERE  name = 'Ground'))
)
ORDER BY name ASC;

3.	Find All Pokémon who has a BST between 200 to 500 who is weak against water types

SELECT p.name
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
WHERE
(t1.id IN
    (SELECT target_type FROM type_weak WHERE source_type = (SELECT id FROM types WHERE  name = 'Water'))
)
OR
(t2.id IN
    (SELECT target_type FROM type_weak WHERE source_type = (SELECT id FROM types WHERE  name = 'Water'))
)
AND (base_stat BETWEEN 200 AND 500)
ORDER BY name ASC;

4.	Find the Pokémon with the highest Atk, has a Mega evolution form and vulnerable to fire

SELECT p.name
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
WHERE
(t1.id IN
    (SELECT target_type FROM type_vulnerable WHERE source_type = (SELECT id FROM types WHERE  name = 'Fire'))
)
OR
(t2.id IN
    (SELECT target_type FROM type_vulnerable WHERE source_type = (SELECT id FROM types WHERE  name = 'Fire'))
)
AND is_mega
ORDER BY p.attack DESC
LIMIT 1;