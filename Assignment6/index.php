<?php
// start the session
session_start();

require_once 'sqlhelper.php';
require_once 'functions.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();

/*
    initialize session to use with favorite pokemons
*/
init_session();

if (isset($_GET["fav"])) {
    add_favorite_pokemon($_GET["fav"]);
}

// load pages
$title = "Pokedex";
$sub_title = "My list of Pokemons";
$popular = get_popular_pokemons();
$favorite = get_favorite_pokemons();
$pokedex = get_pokedex();
$template = $twig->load('main.twig.html');

echo $template->render(array("title" => $title,
    "sub_title" => $sub_title,
    "popular" => $popular,
    "favorite" => $favorite,
    "favorite_ids" => get_favorite_pokemons_ids(),
    "pokedex" => $pokedex));
?>