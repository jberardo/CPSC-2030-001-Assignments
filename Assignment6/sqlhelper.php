<?php

/*
You need this after running a SQL query that
calls a stored procedure.
Procedure calls return multiple results, so the 
extra result needs to be cleared.

Example:
$result = $conn->query("call getWeak('Ivysaur')");
clearConnection($conn);

*/
function clearConnection($conn){
    while($conn->more_results()){
       $conn->next_result();
       $conn->use_result();
    }
}

//DB setup for this web-app
function connect(){
    $user = 'pokedex';
    $pwd = 'pokemon';
    $server = '10.0.0.11';
    $dbname = 'pokedex';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}

function show_sql_error($msg, $conn){
    echo $msg . "<br/>";
    echo mysqli_error($conn);
}

?>