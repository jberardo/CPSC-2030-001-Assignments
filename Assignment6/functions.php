<?php
require_once 'sqlhelper.php';

//twig setup for this web-app
function setupMyTwigEnvironment()
{
    $loader = new Twig_Loader_Filesystem('./templates'); //set to load from the ./templates directory
    $twig = new Twig_Environment($loader, array('debug' => true, 'auto_reload' => true, 'strict_variables' => true)); 
    return $twig;  
}

function get_popular_pokemons() {
    $conn = connect();
    $result = $conn->query("call get_popular_pokemons()");

    if($result) {
        $popular = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Error getting popular Pokemons", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $popular;
}

function init_session() {
    if (!isset($_SESSION["favorites"])) {
        $_SESSION["favorites"] = array();
    }
}

function add_favorite_pokemon($pokemon_id) {
    if (contain_favorite($pokemon_id)) {
        // if already exists, delete from favorites
        $_SESSION["favorites"] = deleteElement($pokemon_id, $_SESSION["favorites"]);
    } else if (count($_SESSION["favorites"]) < 7) {
        // add to favorites if not full
        array_push($_SESSION["favorites"], $pokemon_id);
    }
}

function get_favorite_pokemons() {
    if (sizeof($_SESSION["favorites"]) == 0) {
       return array();
    }

    // get pokemons details from database
    $conn = connect();
    $ids = to_list($_SESSION["favorites"]);
    $result = $conn->query("call get_favorite_pokemons('$ids')");

    if($result) {
        $favorites = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Error getting favorites Pokemons", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $favorites;
}

function get_favorite_pokemons_ids() {
    if (sizeof($_SESSION["favorites"]) == 0) {
       return array();
    } else {
        return $_SESSION["favorites"];
    }
}

function get_pokedex() {
    $conn = connect();
    $result = $conn->query("call get_paginated_pokemon(0, 10)");

    if($result) {
        $pokedex = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Error getting Pokedex", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $pokedex;
}

function contain_favorite($pokemon_id) {
    for ($i = 0; $i < count($_SESSION["favorites"]); $i++) {
        if ($_SESSION["favorites"][$i] == $pokemon_id) {
            return true;
        }
    }
    return false;
}

function to_list($arr) {
    $result = "";

    for ($i = 0; $i < sizeof($arr); $i++) {
        $result = $result . $arr[$i];
        if ($i < sizeof($arr) - 1) $result = $result . ", ";
    }

    return $result;
}

function deleteElement($element, $array){
    $index = array_search($element, $array);
    if($index !== false){
        unset($array[$index]);
    }

    return array_values($array);
}
?>