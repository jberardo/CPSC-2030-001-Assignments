DELIMITER //

CREATE PROCEDURE count_pokemons (IN type_id int)
BEGIN

if (type_id = 0)
then
    SELECT COUNT(id) AS total
    FROM pokemon;
else
    SELECT COUNT(id) AS total
    FROM pokemon
    WHERE type1 = type_id OR type2 = type_id;
end if;

END //

DELIMITER //

CREATE PROCEDURE get_paginated_pokemon (IN start_limit int, IN end_limit int)
BEGIN

SELECT p.id, p.national_id, p.name, p.hp, p.attack, p.defense, p.special_attack, p.special_defense, p.speed, p.base_stat, t1.id as type1_id, t2.id as type2_id, t1.name as type1, t2.name as type2, p.is_mega, p.picture
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
ORDER BY p.national_id
LIMIT start_limit, end_limit;

END //

DELIMITER //

CREATE PROCEDURE get_paginated_pokemon_by_type (IN type_id int, IN start_limit int, IN end_limit int)
BEGIN

SELECT p.id, p.national_id, p.name, p.hp, p.attack, p.defense, p.special_attack, p.special_defense, p.speed, p.base_stat, t1.id as type1_id, t2.id as type2_id, t1.name as type1, t2.name as type2, p.is_mega, p.picture
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
WHERE (t1.id = type_id) OR (t2.id = type_id)
ORDER BY p.national_id
LIMIT start_limit, end_limit;

END //

DELIMITER //

CREATE PROCEDURE get_pokemon_detail (IN pid int)
BEGIN

SELECT p.id, p.national_id, p.name, p.hp, p.attack, p.defense, p.special_attack, p.special_defense, p.speed, p.base_stat, t1.id as type1_id, t2.id as type2_id, t1.name as type1, t2.name as type2, p.is_mega, p.picture
FROM pokemon p
INNER JOIN types t1 ON t1.id = p.type1
LEFT JOIN types t2 ON t2.id = p.type2
WHERE p.id = pid
ORDER BY p.national_id;

END //

DELIMITER //

CREATE PROCEDURE get_strong_against (IN type1_id int, IN type2_id int)
BEGIN

    SELECT name FROM types WHERE types.id IN (
        SELECT DISTINCT target_type from type_strong
        LEFT JOIN types t ON t.id = type_strong.source_type
        WHERE
        source_type = type1_id OR source_type = type2_id
    )
    ORDER BY name;

END //

DELIMITER //

CREATE PROCEDURE get_weak_against (IN type1_id int, IN type2_id int)
BEGIN

    SELECT name FROM types WHERE types.id IN (
        SELECT DISTINCT target_type from type_weak
        LEFT JOIN types t ON t.id = type_weak.source_type
        WHERE
        source_type = type1_id OR source_type = type2_id
    )
    ORDER BY name;

END //

DELIMITER //

CREATE PROCEDURE get_resistant_to (IN type1_id int, IN type2_id int)
BEGIN

    SELECT name FROM types WHERE types.id IN (
        SELECT DISTINCT target_type from type_resistant
        LEFT JOIN types t ON t.id = type_resistant.source_type
        WHERE
        source_type = type1_id OR source_type = type2_id
    )
    ORDER BY name;

END //

DELIMITER //

CREATE PROCEDURE get_vulnerable_to (IN type1_id int, IN type2_id int)
BEGIN

    SELECT name FROM types WHERE types.id IN (
        SELECT DISTINCT target_type from type_vulnerable
        LEFT JOIN types t ON t.id = type_vulnerable.source_type
        WHERE
        source_type = type1_id OR source_type = type2_id
    )
    ORDER BY name;

END //

DELIMITER //

CREATE PROCEDURE get_favorite_pokemons (IN pokemon_ids VARCHAR(255))
READS SQL DATA
BEGIN


  SET @query = CONCAT(
      'SELECT p.id, p.national_id, p.name, p.hp, p.attack, p.defense, p.special_attack, p.special_defense, p.speed, p.base_stat, t1.id as type1_id, t2.id as type2_id, t1.name as type1, t2.name as type2, p.is_mega, p.picture
      FROM pokemon p
      INNER JOIN types t1 ON t1.id = p.type1
      LEFT JOIN types t2 ON t2.id = p.type2      
      WHERE p.id IN (', pokemon_ids, ')');

  PREPARE stmt FROM @query;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;

END //

DELIMITER //

CREATE PROCEDURE get_popular_pokemons ()
READS SQL DATA
BEGIN

SELECT p.id, p.name FROM pokemon p
INNER JOIN popular po ON po.pokemon_id = p.id
ORDER BY po.rank ASC;

END //