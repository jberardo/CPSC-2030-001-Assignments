USE pokedex;

/*
    Table to represent a pokemon type (grass, poison, etc)
*/
CREATE TABLE types (
    id int NOT NULL,
    name varchar(50) NOT NULL,
    PRIMARY KEY (id)
);

/*
    Table to represent a pokemon
*/
CREATE TABLE pokemon (
  id int NOT NULL AUTO_INCREMENT,
  national_id int NOT NULL,
  name varchar(50) NOT NULL,
  hp int NOT NULL,
  attack int NOT NULL,
  defense int NOT NULL,
  special_attack int NOT NULL,
  special_defense int NOT NULL,
  speed int NOT NULL,
  base_stat int NOT NULL,
  type1 int NOT NULL,
  type2 int,
  is_mega int NOT NULL,
  picture varchar(255),
  PRIMARY KEY(id),
  FOREIGN KEY (type1) REFERENCES types (id),
  FOREIGN KEY (type2) REFERENCES types (id)
);

/*
    Table to map which type (source) is strong against (target)
*/
CREATE TABLE type_strong (
    source_type int NOT NULL,
    target_type int NOT NULL,
    PRIMARY KEY (source_type, target_type),
    FOREIGN KEY (source_type) REFERENCES types (id),
    FOREIGN KEY (target_type) REFERENCES types (id)
);

/*
    Table to map which type (source) weak against (target)
*/
CREATE TABLE type_weak (
    source_type int NOT NULL,
    target_type int NOT NULL,
    PRIMARY KEY (source_type, target_type),
    FOREIGN KEY (source_type) REFERENCES types (id),
    FOREIGN KEY (target_type) REFERENCES types (id)
);

/*
    Table to map which type (source) is vulnerable to (target)
*/
CREATE TABLE type_vulnerable (
    source_type int NOT NULL,
    target_type int NOT NULL,
    PRIMARY KEY (source_type, target_type),
    FOREIGN KEY (source_type) REFERENCES types (id),
    FOREIGN KEY (target_type) REFERENCES types (id)
);

/*
    Table to map which type (source) is resistant to (target)
*/
CREATE TABLE type_resistant (
    source_type int NOT NULL,
    target_type int NOT NULL,
    PRIMARY KEY (source_type, target_type),
    FOREIGN KEY (source_type) REFERENCES types (id),
    FOREIGN KEY (target_type) REFERENCES types (id)
);