USE pokedex;

DROP TABLE pokemon;
DROP TABLE type_strong;
DROP TABLE type_weak;
DROP TABLE type_resistant;
DROP TABLE type_vulnerable;
DROP TABLE types;