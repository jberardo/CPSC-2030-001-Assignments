<?php
require_once 'sqlhelper.php';

include 'header.php';

if (isset($_GET["page"])) {$page = $_GET["page"];} else {$page=1;}
if (isset($_GET["type_id"])) { $type_id = $_GET["type_id"];} else {$type_id = -1;}

$start_from = ($page-1) * $results_per_page;

if ($type_id != -1) {
    $pokedex = mysql_exec_proc("call get_paginated_pokemon_by_type(\"$type_id\", \"$start_from\", \"$results_per_page\")"); 
    $total_items = mysql_get_row("call count_pokemons(\"$type_id\")")["total"];
} else {
    $pokedex = mysql_exec_proc("call get_paginated_pokemon(\"$start_from\", \"$results_per_page\")");
    $total_items = mysql_get_row("call count_pokemons(0)")["total"];
}

$total_pages = ceil($total_items / $results_per_page);

$pagination = "";
for ($i=1; $i<=$total_pages; $i++) {
    $pagination .= "<a href=index.php?page=".$i."";
    if ($type_id > 0) $pagination .= "&type_id=".$type_id."";
    if ($i==$page) $pagination .= " class='active'";
    $pagination .= ">".$i."</a> ";
};
?>

<div class="title center">
    <h1><a href="index.php">Pokedex</a></h1>
    <h2>List of Pokemons</h2>
</div>

<div class="center">
    <div class="pagination">
        <?= $pagination ?>
    </div>
</div>

<div class="container">
    <?php foreach ($pokedex as $p) { ?>
        <div class="pokecard">
            <a href="details.php?id=<?=$p["id"]?>"><img alt="Pokemon - <?= $p["picture"] ?>" src="<?= $p["picture"] ?>"></a>
            <h2>
                <?= $p["name"] ?> (<?= $p["national_id"] ?>)
            </h2>
            <p>
                <a href="index.php?type_id=<?=$p["type1_id"]?>"><?= $p["type1"]?></a>
                <a href="index.php?type_id=<?=$p["type2_id"]?>"><?= $p["type2"]?></a>
            </p>
        </div>
    <?php } ?>
</div>

<div class="center">
    <div class="pagination">
        <?= $pagination ?>
    </div>
</div>
<?php
include 'footer.php';
?>