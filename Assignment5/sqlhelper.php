<?php
$results_per_page = 20; // number of results per page

function connect(){
    $user = 'CPSC2030';
    $pwd = 'CPSC2030'; // dummy password so this file can be uploaded to gitlab
    $server = '10.0.0.11';
    $dbname = 'pokedex';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}

function mysql_exec_proc($query) {
    $conn = connect();

    $result = $conn->query($query);

    if ($result) {
        $data = $result->fetch_all(MYSQLI_ASSOC);
        $conn->close();
        return $data;
    } else {
        display_error(mysqli_error());
    }
}

function mysql_get_row($query,$y=0) {
    $conn = connect();

    $result = $conn->query($query);

    if ($result) {
        $row = $result->fetch_all(MYSQLI_ASSOC);
        $conn->close();
        $rec = $row[$y];
        return $rec;
    } else {
        display_error(mysql_error());
    }
}

function display_error($message) {
    die("Could not run query: " . $message);
}
?>