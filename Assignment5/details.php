<?php
require_once 'sqlhelper.php';
include 'header.php';

$pokemon_id = $_GET["id"];
$pokemon = mysql_get_row("call get_pokemon_detail(\"$pokemon_id\")");

$type1 = $pokemon["type1_id"];
$type2 = $pokemon["type2_id"];

if ($type2 > 0) {
    $type_strong = mysql_exec_proc("call get_strong_against(\"$type1\", \"$type2\")");
    $type_weak = mysql_exec_proc("call get_weak_against(\"$type1\", \"$type2\")");
    $type_resistant = mysql_exec_proc("call get_resistant_to(\"$type1\", \"$type2\")");
    $type_vulnerable = mysql_exec_proc("call get_vulnerable_to(\"$type1\", \"$type2\")");
} else {
    $type_strong = mysql_exec_proc("call get_strong_against(\"$type1\", 0)");
    $type_weak = mysql_exec_proc("call get_weak_against(\"$type1\", 0)");
    $type_resistant = mysql_exec_proc("call get_resistant_to(\"$type1\", 0)");
    $type_vulnerable = mysql_exec_proc("call get_vulnerable_to(\"$type1\", 0)");
}

?>

<div class="center">
    <div class="title center">
        <h1><a href="index.php">Pokedex</a></h1>
        <h2>Details for <?=$pokemon["name"]?></h2>
    </div>

    <div class="pokemon_details">
        <a href="details.php?id=<?=$pokemon["id"]?>"><img alt="Pokemon - <?= $pokemon["picture"] ?>" src="<?= $pokemon["picture"] ?>"></a>

        <h2><?= $pokemon["name"] ?> (<?= $pokemon["national_id"] ?>)</h2>

        <p>
            <a href="index.php?type_id=<?=$pokemon["type1_id"]?>"><?= $pokemon["type1"]?></a>
            <a href="index.php?type_id=<?=$pokemon["type2_id"]?>"><?= $pokemon["type2"]?></a>
        </p>
    </div>

    <div class="type_chart">
        Strong:
        <?php foreach ($type_strong as $strong) {
             echo $strong["name"] . " ";
        }?>
        <br />

        Weak:
        <?php foreach ($type_weak as $weak) {
             echo $weak["name"] . " ";
        }?>
        <br />

        Resistant:
        <?php foreach ($type_resistant as $resistant) {
             echo $resistant["name"] . " ";
        }?>
        <br />

        Vulnerble:
        <?php foreach ($type_vulnerable as $vulnerable) {
             echo $vulnerable["name"] . " ";
        }?>
        <br />
    </div>
</div>

<?php
include 'footer.php';
?>