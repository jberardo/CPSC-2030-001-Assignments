<?php
require_once 'database.php';

function login($username, $password) {
    $ret = false;
    $conn = connect();
    $result = $conn->query("call login('$username', '$password')");
    $user = array();

    if($result) {
        $user = $result->fetch_all(MYSQLI_ASSOC)[0];
        $_SESSION["user"] = $user;
    } else {
        $ret = false;
        $_SESSION["user"] = "";
    }

    clearConnection($conn);

    if (!empty($user))
    {
        $ret = true;
    }

    return $ret;
}

function register($fname, $lname, $email, $password) {
    $conn = connect();
    $retval = false;

    $result = $conn->query("call register('$fname', '$lname', '$email', '$password')");

    if($result) {
        $_SESSION["user"] = $result->fetch_all(MYSQLI_ASSOC)[0];
        $retval = $_SESSION["user"];
    } else {
        //show_sql_error("Error during sign up", $conn);
        //$_SESSION["user"] = "";
        reset_session();
        $retval = false;
    }

    clearConnection($conn);

    return $retval;
}

function reset_session()
{
    $_SESSION["user"] = "";
}

function get_user()
{
    if (logged())
    {
        return $_SESSION["user"];
    }
    else
    {
        return array();
    }
}

function logged()
{
    if (empty($_SESSION["user"]))
    {
        return false;
    }
    else
    {
        return true;        
    }
}


 function get_memories($user_id)
 {
    if (!logged())
    {
        return false;
    }
    else
    {
        $conn = connect();
        $retval = false;

        $result = $conn->query("call get_memories($user_id)");

        if($result) {
            $retval = $result->fetch_all(MYSQLI_ASSOC);
        }

        clearConnection($conn);

        return $retval;    
    }
 }

/*
CHAT FUNCTIONS
*/
function get_recent_messages($id) {
    $conn = connect();

    if ($id)
    {
        $result = $conn->query("call get_messages_after($id)");
    }
    else {
        $result = $conn->query("call get_recent_messages()");
    }

    if($result) {
        $msgs = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Error retrieving messages", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $msgs;
}

function add_message($username, $message) {
    $conn = connect();
    $result = $conn->query("call add_message('$username', '$message')");
    
    if($result) {
        $msg = $result->fetch_all(MYSQLI_ASSOC);
        clearConnection($conn);
    } else {
        show_sql_error("Message could not be sent", $conn);
        $conn->close();
        die();
    }

    $conn->close();

    return $msg[0];
}

function generateJSON($messages)
{
        if ($messages)
        {
            $out = array();

            foreach ($messages as $msg)
            {
                array_push($out, array(
                    "id"=>$msg["id"],
                    "sent"=>$msg["sent"],
                    "username"=>$msg["username"],
                    "message"=>$msg["content"]
                ));
            }

            echo json_encode($out);
        }
        else
        {
            send_msg("No message found");
        }
}

function send_msg($msg)
{
    echo json_encode(array(
        "id" => 0,
        "sent" => 0,
        "username" => "",
        "message" => $msg
    ));
}
?>