<?php

//DB setup for this web-app
function connect(){
    $user = 'CPSC2030';
    $pwd = 'CPSC2030';
    $server = 'localhost';
    $dbname = 'memcorp';
 
    $conn = new mysqli($server, $user, $pwd, $dbname);
    return $conn;
}

/*
You need this after running a SQL query that
calls a stored procedure.
Procedure calls return multiple results, so the 
extra result needs to be cleared.

Example:
$result = $conn->query("call getWeak('Ivysaur')");
clearConnection($conn);

*/
function clearConnection($conn){
    while($conn->more_results()){
       $conn->next_result();
       $conn->use_result();
    }
}

function show_sql_error($msg, $conn){
    echo $msg . "<br/>";
    echo mysqli_error($conn);
}
?>