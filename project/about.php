<?php
// start the session
session_start();

require_once 'twig.php';
require_once 'functions.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();

// load pages
$title = "Welcome to Mem Corp";
$sub_title = "Where your memory remains...";

$user = get_user();

$template = $twig->load('about.twig.html');

echo $template->render(array("title" => $title,
    "sub_title" => $sub_title,
        "user" => $user));
?>