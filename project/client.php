<?php
// start the session
session_start();

require_once 'twig.php';
require_once 'functions.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();

$user = get_user();

// load pages
$title = "Welcome to Mem Corp";
$sub_title = "Where your memory remains...";

if (!$user){
    $template = $twig->load('login.twig.html');
    echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "msg" => "Please enter your username and password"));
} else {
    $memories = get_memories($user["id"]);
    $template = $twig->load('client.twig.html');
    echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "user" => $user,
        "memories" => $memories));
}
?>