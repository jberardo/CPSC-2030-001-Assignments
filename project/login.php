<?php
// start the session
session_start();

require_once 'functions.php';
require_once 'database.php';
require_once 'twig.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();
$user = get_user();

$title = "Welcome to Mem Corp";
$sub_title = "Where your memory remains...";

if (isset($_POST["username"]) and isset($_POST["password"])) {
    $username = $_POST["username"];
    $password = $_POST["password"];

    if (login($username, $password)) {
        $template = $twig->load('client.twig.html');
        echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "user" => get_user(),
        "memories" => false));
    } else {
        $template = $twig->load('login.twig.html');
        echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "msg" => "Invalid username or password"));
    }

} else {
        $template = $twig->load('login.twig.html');
        echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "msg" => "Please enter your username and password"));
}
?>