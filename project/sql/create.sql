CREATE DATABASE memcorp;

/*
    TABLES
*/
CREATE TABLE user (
    id int NOT NULL AUTO_INCREMENT,
    fname varchar(50) NOT NULL,
    lname varchar(50) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    active bit NOT NULL DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE memory (
    id int NOT NULL AUTO_INCREMENT,
    user_id int NOT NULL,
    path varchar(255),
    created_at date,
    updated_at date,
    PRIMARY KEY (id),
    FOREIGN KEY (user_id) REFERENCES user (id)
);

CREATE TABLE messages (
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50),
    sent TIMESTAMP,
    content VARCHAR(255),
    PRIMARY KEY (id)
);

/*
    STORED PROCEDURES
*/
DELIMITER //

CREATE PROCEDURE get_recent_messages ()
BEGIN

SELECT id, username, sent, content
FROM messages
WHERE sent >= (NOW() - INTERVAL 1 DAY)
ORDER BY sent ASC
LIMIT 10;

END //

DELIMITER //

CREATE PROCEDURE get_messages_after (IN msg_id int)
BEGIN

SELECT id, username, sent, content
FROM messages
WHERE id > msg_id
ORDER BY sent ASC;

END //

DELIMITER //

CREATE PROCEDURE add_message (IN username varchar(50), IN content varchar(255))
BEGIN

INSERT INTO messages (username, content) VALUES (username, content);
SELECT id, sent, username, content FROM messages WHERE id = LAST_INSERT_ID();

END //

DELIMITER //

CREATE PROCEDURE login (IN iemail varchar(255), IN ipassword varchar(255))
BEGIN

SELECT * FROM user
WHERE email = iemail AND password = ipassword;

END //

DELIMITER //

CREATE PROCEDURE register (IN fname varchar(50), IN lname varchar(50), IN email varchar(255), IN password varchar(255))
BEGIN

INSERT INTO user (fname, lname, email, password) VALUES (fname, lname, email, password);
SELECT id, fname, lname, email, password FROM user WHERE id = LAST_INSERT_ID();

END //

DELIMITER //

CREATE PROCEDURE get_memories (IN _id int)
BEGIN

SELECT * FROM memory WHERE user_id = _id;

END //

INSERT INTO user (fname, lname, email, password)
VALUES ('John', 'Doe', 'john@doe.com', 'test');
INSERT INTO user (fname, lname, email, password)
VALUES ('Jane', 'Doe', 'jane@doe.com', 'test');
INSERT INTO user (fname, lname, email, password)
VALUES ('Test', 'Ing', 'test', 'test');

INSERT INTO memory (user_id, path)
VALUES (1, 'img/memories/1/1.jpg');
INSERT INTO memory (user_id, path)
VALUES (1, 'img/memories/1/2.jpg');
INSERT INTO memory (user_id, path)
VALUES (1, 'img/memories/1/3.jpg');
INSERT INTO memory (user_id, path)
VALUES (1, 'img/memories/1/4.jpg');
INSERT INTO memory (user_id, path)
VALUES (1, 'img/memories/1/5.jpg');

INSERT INTO messages (username, content)
VALUES ('john@doe.com', 'tesing 1');
INSERT INTO messages (username, content)
VALUES ('jane@doe.com', 'tesing 2');
INSERT INTO messages (username, content)
VALUES ('test', 'it works!');