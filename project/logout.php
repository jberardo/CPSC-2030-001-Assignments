<?php
// start the session
session_start();

require_once './vendor/autoload.php';
require_once 'database.php';
require_once 'functions.php';
require_once 'twig.php';

reset_session();

$twig = setupMyTwigEnvironment();

// load pages
$title = "Welcome to Mem Corp";
$sub_title = "Where your memory remains...";
$template = $twig->load('index.twig.html');

echo $template->render(array("title" => $title,
    "sub_title" => $sub_title));
?>