<?php
// start the session
session_start();

require_once 'functions.php';
require_once 'twig.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();

// load pages
$title = "Welcome to Mem Corp";
$sub_title = "Share your memories real time!";

$user = get_user();

if (!$user)
{
    $template = $twig->load('login.twig.html');
    echo $template->render(array("title" => $title,
    "sub_title" => $sub_title,
    "msg" => "Please enter your username and password"));
}
else
{
    $template = $twig->load('share.twig.html');
    echo $template->render(array("title" => $title,
        "sub_title" => $sub_title,
        "user" => $user));
}
?>