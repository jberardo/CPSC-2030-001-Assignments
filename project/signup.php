<?php

require_once 'database.php';
require_once 'functions.php';

// start the session
session_start();

require_once 'twig.php';
require_once './vendor/autoload.php';

$twig = setupMyTwigEnvironment();

$title = "Welcome to Mem Corp";
$sub_title = "Where your memory remains...";

if (isset($_POST["fname"]) and isset($_POST["lname"]) and isset($_POST["email"]) and isset($_POST["password"])) {

    $fname = $_POST["fname"];
    $lname = $_POST["lname"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    
    $user = register($fname, $lname, $email, $password);

    if ($user) {
        $template = $twig->load('client.twig.html');
        echo $template->render(array("title" => $title,
            "sub_title" => $sub_title,
            "user" => $user,
            "memories" => false));
    } else {
        $template = $twig->load('signup.twig.html');
        echo $template->render(array("title" => $title,
            "sub_title" => $sub_title,
            "msg" => "Error during sign up. Please try again"));
    }
} else {
        $template = $twig->load('signup.twig.html');
        echo $template->render(array("title" => $title,
            "sub_title" => $sub_title,
            "msg" => "Please enter your info below"));    
}
?>