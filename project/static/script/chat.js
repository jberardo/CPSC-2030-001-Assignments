init();

// initialize chat settings, events, etc
function init() {
    // when user hits enter, process message and clear input text
    $("#msg").keypress(function (event) {
        send_message(event);
    });

    $("#username").keypress(function (event) {
        send_message(event);
    });

    // fetch last 10 messages in the last hour
    // fetchdata() will check if the list of messages if it is empty
    // since this first call, the #messages div will be empty
    // and will return 10 last msgs
    fetchdata();

    // set interval to call fetchdata every second
    // subsequent calls to fetchdata() will return all messages
    // that were sent after the last message that the user has seen
    $(document).ready(function () {
        setInterval(fetchdata, 1000);
    });
}

function send_message(event) {
    if (event.keyCode == 13) {
        if ($("#username").val() === "") {
            alert("Enter your username");
            $("#username").focus();
            return;
        }

        if ($("#msg").val() === "") {
            alert("Please enter a message");
            $("#msg").focus();
            return;
        }

        process_message($("#msg").val());
        $("#msg").val("");
    }
}

function process_message(message) {
    let username = $("#username").val();

    $.ajax({
        url: "chat.php",
        method: "POST",
        data: {
            username: username,
            message: message
        },
        success: function (ret) {
            append_message(JSON.parse(ret));
        }
    });
}

function append_message(message) {
    $(".item").removeClass("selected");

    $("#messages").append(
        '<p idx=' + message.id + ' class="item selected">[' +
        '<span class="sent">' + message.sent + "</span>" +
        "] " +
        '<span class="username">' + message.username + "</span>" +
        ": " +
        '<span class="content">' + message.message + "</span>" +
        "</p>"
    );

    $("#messages").animate({
            scrollTop: $(document).height()
        },
        "fast"
    );
}

// this function will be called in 2 situations
// 1: at the begining
// 2: every second
// first we check if the list of messages is empty
// if it is, it's the first call, so we retrieve the recent messages
// recent messages = last 10 msgs in last hours
// if there is at least one message, we get the id of this message
// and send it through POST method to server.php
// the server will call the apropriate stored procedure to get the correct messages
function fetchdata() {
    id = $("div.messages > p:last").attr("idx"); // get idx of last <p> inside messages div
    console.log(id);
    if (id > 0) {
        $.ajax({
            url: "chat.php",
            method: "POST",
            data: {
                id: id
            },
            success: function (ret) {
                let out = "";
                let obj = JSON.parse(ret);
                for (let i = 0; i < obj.length; i++) {
                    append_message(obj[i]);
                }
            }
        });
    } else {
        $.ajax({
            url: "chat.php",
            method: "GET",
            success: function (ret) {
                let out = "";

                if (isValid(ret)) {
                    let obj = JSON.parse(ret);
                    for (let i = 0; i < obj.length; i++) {
                        append_message(obj[i]);
                    }
                } else {
                    // error parsing json from server.php!!
                    // show the error to the user (to debug)
                    alert(ret);
                }
            }
        });
    }
}

// check if json returned by server is valid or not
function isValid(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}