"use restrict";

let battalion_characters = [{
        ID: 1,
        Name: "Claude Wallace",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "First Lieutenant",
        Role: "Tank Commander",
        Class: "Scout",
        Description: "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
        Image: "http://valkyria.sega.com/img/character/chara-ss01.jpg"
    },
    {
        ID: 2,
        Name: "Riley Miller",
        Side: "Edinburgh Army",
        Unit: "Federate Joint Ops",
        Rank: "Second Lieutenant",
        Role: "Artillery Advisor",
        Class: "Grenadier",
        Description: "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",
        Image: "http://valkyria.sega.com/img/character/chara-ss02.jpg"
    },
    {
        ID: 3,
        Name: "Raz",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant",
        Role: "Fireteam Leader",
        Class: "Shocktrooper",
        Description: "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.",
        Image: "http://valkyria.sega.com/img/character/chara-ss03.jpg"
    },
    {
        ID: 4,
        Name: "Kai Schulen",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant Major",
        Role: "Fireteam Leader",
        Class: "Sniper",
        Description: 'Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename "Deadeye Kai". Along with her childhood friends, she joined a foreign military to take the fight to the Empire.She loves fresh - baked bread, almost to a fault.',
        Image: "http://valkyria.sega.com/img/character/chara-ss04.jpg"
    },
    {
        ID: 5,
        Name: "Minerva Victor",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad F",
        Rank: "First Lieutenant",
        Role: "Senior Commander",
        Class: "Scout",
        Description: "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
        Image: "http://valkyria.sega.com/img/character/chara-ss11.jpg"
    },
    {
        ID: 6,
        Name: "Karen Stuart",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "Corporal",
        Role: "Combat EMT",
        Class: "Medic",
        Description: "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",
        Image: "http://valkyria.sega.com/img/character/chara-ss12.jpg"
    },
    {
        ID: 7,
        Name: "Ragnarok",
        Side: "Edinburgh Army",
        Unit: "Squad E",
        Rank: "K - 9 Unit",
        Role: "Mascot",
        Class: "Medic",
        Description: "Once a stray, this good good boy is lovingly referred to as \"Rags\". As a K - 9 unit, he 's a brave and intelligent rescue dog who's always willing to lend a helping paw.When the going gets tough, the tough get ruff.",
        Image: "http://valkyria.sega.com/img/character/chara-ss13.jpg"
    },
    {
        ID: 8,
        Name: "Miles Arbeck",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Sergeant",
        Role: "Tank Operator",
        Class: "Driver",
        Description: "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
        Image: "http://valkyria.sega.com/img/character/chara-ss15.jpg"
    },
    {
        ID: 9,
        Name: "Dan Bentley",
        Side: "Edinburgh Army",
        Unit: "Ranger Corps, Squad E",
        Rank: "Private First Class",
        Role: "APC Operator",
        Class: "Driver",
        Description: "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat.",
        Image: "http://valkyria.sega.com/img/character/chara-ss16.jpg"
    },
    {
        ID: 10,
        Name: "Roland Morgen",
        Side: "Edinburgh Navy",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Ship 's Captain",
        Role: "Cruiser Commander",
        Class: "Officer",
        Description: "Born in the United Kingdom of Edinburgh, this naval officer commands a state-of-the-art snow cruiser named the Centurion. For a ship's captain, his disposition is surprisingly mild-mannered. As such, he never loses his composure, even in the direst of straits.",
        Image: "http://valkyria.sega.com/img/character/chara-ss18.jpg"
    },
    {
        ID: 11,
        Name: "Marie Bennett",
        Side: "Edinburgh Navy",
        Unit: "Centurion, Cygnus Fleet",
        Rank: "Petty Officer",
        Role: "Chief of Operations",
        Class: "Veteran",
        Description: "As the Centurion's crewmember responsible for overseeing daily operations, this gentle and supportive EWI veteran even takes daily tasks like cooking and cleaning upon herself. She never forgets to wear a smile. Her age is undisclosed, even in her personnel files.",
        Image: "http://valkyria.sega.com/img/character/chara-ss20.jpg"
    }
];

function create_battalion_list(character_id, name) {
    var ul = document.getElementById("battalion_list");
    var li = document.createElement("li");

    var link = document.createElement("a");
    link.text = name;
    link.href = "#";
    link.setAttribute("id", character_id);
    link.onclick = add_squad;

    link.addEventListener(
        "mouseover",
        function (e) {
            set_profile_info(e.target.id);
        },
        false
    );

    li.appendChild(link);

    ul.appendChild(li);
}

function add_squad(e) {
    character_id = e.target.id;
    name = e.target.text;

    if (!is_valid_squad(character_id)) {
        return;
    }

    var ul = document.getElementById("squad_list");
    var li = document.createElement("li");

    var link = document.createElement("a");
    link.text = name;
    link.href = "#";

    li.addEventListener(
        "mouseover",
        function (e) {
            set_profile_info(e.currentTarget.id);
        },
        false
    );

    li.setAttribute("id", character_id);
    li.onclick = delete_squad;

    li.appendChild(link);
    ul.appendChild(li);
}

function is_valid_squad(character_id) {
    var ul = document.getElementById("squad_list");
    var li = ul.getElementsByTagName("li");

    if (li.length >= 5) {
        alert("Squad full!");
        return false;
    }

    for (var i = 0; i < li.length; i++) {
        if (character_id == parseInt(li[i].getAttribute("id"), 10)) {
            alert("Character already in Squad!");
            return false;
        }
    }

    return true;
}

function delete_squad(e) {
    var character_id = e.currentTarget.id;
    var ul = document.getElementById("squad_list");
    var li = ul.getElementsByTagName("li");

    for (var i = 0; i < li.length; i++) {
        if (character_id == parseInt(li[i].getAttribute("id"), 10)) {
            ul.removeChild(li[i]);
        }
    }
}

function set_profile_info(character_id) {
    var name = document.getElementById("profile_name");
    var side = document.getElementById("profile_side");
    var unit = document.getElementById("profile_unit");
    var rank = document.getElementById("profile_rank");
    var role = document.getElementById("profile_role");
    var clazz = document.getElementById("profile_class");
    var description = document.getElementById("profile_description");
    var image = document.getElementById("profile_image");

    battalion_characters.forEach(element => {
        if (element.ID == character_id) {
            name.innerHTML = element.Name;
            side.innerHTML = element.Side;
            unit.innerHTML = element.Unit;
            rank.innerHTML = element.Rank;
            role.innerHTML = element.Role;
            clazz.innerHTML = element.Class;
            description.innerHTML = element.Description;
            image.src = element.Image;
        }
    });
}

battalion_characters.forEach(element => {
    create_battalion_list(element.ID, element.Name);
});