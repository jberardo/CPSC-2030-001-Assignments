function toggle_open(event) {
    $(".menu").toggleClass("open");
    $(".menu_main").toggleClass("open");
    $(".ball").toggleClass("open");
}

function toggle_ball(event) {
    let coordinate = $(event.currentTarget).offset();

    $(".ball").animate({
        left: coordinate.left + 'px'
    });

    location.href = event.target.href;
}

$(".menu_main").click(toggle_open);
$(".item").click(toggle_ball);